package org.btssio.siogel;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ClientAdapter extends BaseAdapter {

    ArrayList<Client> listeClients;
    LayoutInflater layoutInflater;

    @Override
    public int getCount() {
        return listeClients.size();
    }

    @Override
    public Object getItem(int position) {
        return listeClients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class Conteneur {
        TextView txtId, txtNom, txtPrenom, txtTelephone, txtAdresse, txtCodePostal, txtVille;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Conteneur contenu;
        if (convertView == null) {
            contenu = new Conteneur();
            convertView = layoutInflater.inflate(R.layout.vue_client, null);
            contenu.txtId = (TextView) convertView.findViewById(R.id.identifiant);
            contenu.txtNom = (TextView) convertView.findViewById(R.id.nom);
            contenu.txtPrenom = (TextView) convertView.findViewById(R.id.prenom);
            contenu.txtTelephone = (TextView) convertView.findViewById(R.id.telephone);
            contenu.txtAdresse = (TextView) convertView.findViewById(R.id.adresse);
            contenu.txtCodePostal = (TextView) convertView.findViewById(R.id.cp);
            contenu.txtVille = (TextView) convertView.findViewById(R.id.ville);
            convertView.setTag(contenu);
        }
        else {
            contenu = (Conteneur) convertView.getTag();
        }
        contenu.txtId.setText(""+listeClients.get(position).getIdentifiant());
        contenu.txtNom.setText(""+listeClients.get(position).getNom());
        contenu.txtPrenom.setText(""+listeClients.get(position).getPrenom());
        String tel = listeClients.get(position).getTelephone();
        tel= String.format("%s.%s.%s.%s.%s",
                tel.substring(0,2),
                tel.substring(2,4),
                tel.substring(4,6),
                tel.substring(6,8),
                tel.substring(8,10));
        contenu.txtTelephone.setText(tel);
        contenu.txtAdresse.setText(""+listeClients.get(position).getAdresse());
        contenu.txtCodePostal.setText(""+listeClients.get(position).getCodePostal());
        contenu.txtVille.setText(""+listeClients.get(position).getVille());

        if (position%2==0) {
            convertView.setBackgroundColor(Color.rgb(238,233,233));
        }
        else
        {
            convertView.setBackgroundColor(Color.rgb(255,255,255));
        }
        return convertView;
    }

    public ClientAdapter(Context context, ArrayList<Client> listeClients) {
            this.layoutInflater = LayoutInflater.from(context);
            this.listeClients = listeClients;
    }

}

