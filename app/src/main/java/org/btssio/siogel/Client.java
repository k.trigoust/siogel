package org.btssio.siogel;

public class Client {
    // Données qui ne seront pas « modifiables » par le livreur
    private int identifiant;
    private String nom, prenom, adresse, codePostal, ville, telephone;
    private String dateCommande;
    private String heureSouhaitLiv; // créneau horaire de livraison souhaitée par le client
    // Données « modifiables », qui seront à saisir lors d’une visite
    private String heureReelleLivraison; // pour simplifier l’heure sera une chaîne
    private String dateLivraison; // pour simplifier la date sera une chaîne
    private String signatureBase64;
    private int etat;
    /* Exemples de situation client
     * 1 Absent lors de toutes les présentations du livreur
     * 2 Présent, mais refus de prendre livraison
     * 3 Présent et tout ok = livré
     */

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getHeureSouhaitLiv() {
        return heureSouhaitLiv;
    }

    public void setHeureSouhaitLiv(String heureSouhaitLiv) {
        this.heureSouhaitLiv = heureSouhaitLiv;
    }

    public String getHeureReelleLivraison() {
        return heureReelleLivraison;
    }

    public void setHeureReelleLivraison(String heureReelleLivraison) {
        this.heureReelleLivraison = heureReelleLivraison;
    }

    public String getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(String dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getSignatureBase64() {
        return signatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        this.signatureBase64 = signatureBase64;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Client() {
        this.identifiant = 0;
        this.nom = "NOM";
        this.prenom = "Prénom";
        this.adresse = "1 Place Saint-Pierre";
        this.codePostal = "72000";
        this.ville = "Le Mans";
        this.telephone = "0243474747";
        this.dateCommande = "00/00/0000";
        this.heureSouhaitLiv = "00h00";
        this.heureReelleLivraison = null;
        this.dateLivraison = null;
        this.signatureBase64 = null;
        this.etat = 0;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String dateCommande, String heureSouhaitLiv) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.dateCommande = dateCommande;
        this.heureSouhaitLiv = heureSouhaitLiv;
        this.heureReelleLivraison = null;
        this.dateLivraison = null;
        this.signatureBase64 = null;
        this.etat = 0;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String dateCommande, String heureSouhaitLiv, String heureReelleLivraison, String dateLivraison, String signature, int etat) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.dateCommande = dateCommande;
        this.heureSouhaitLiv = heureSouhaitLiv;
        this.heureReelleLivraison = heureReelleLivraison;
        this.dateLivraison = dateLivraison;
        this.signatureBase64 = signature;
        this.etat = etat;
    }

    @Override
    public String toString() {
        return "Client{" +
                "identifiant=" + identifiant +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", telephone='" + telephone + '\'' +
                ", dateCommande='" + dateCommande + '\'' +
                ", heureSouhaitLiv='" + heureSouhaitLiv + '\'' +
                ", heureReelleLivraison='" + heureReelleLivraison + '\'' +
                ", dateLivraison='" + dateLivraison + '\'' +
                ", signatureBase64='" + signatureBase64 + '\'' +
                ", etat=" + etat +
                '}';
    }
}
