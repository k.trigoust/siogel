package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModificationClient extends AppCompatActivity implements View.OnClickListener {

    TextView editTextId, editTextNom, editTextPrenom, editTextTel, editTextAdr, editTextCP, editTextVille, editTextDateC, editTextHeureS;
    EditText editTextDateL, editTextHeureL, editTextEtat;
    Button annule, enreg, geoloc;
    int id, etat;
    Intent intentAfficheListeClients, intentGeolocalisation;
    ClientDAO client = new ClientDAO(this);
    Client c;
    String heureL, dateL;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_client);
        id = this.getIntent().getExtras().getInt("id");
        c = client.getClient(id);

        editTextId = (TextView) this.findViewById(R.id.editTextId);
        editTextNom = (TextView) this.findViewById(R.id.editTextNom);
        editTextPrenom = (TextView) this.findViewById(R.id.editTextPrenom);
        editTextTel = (TextView) this.findViewById(R.id.editTextTel);
        editTextAdr = (TextView) this.findViewById(R.id.editTextAdr);
        editTextCP = (TextView) this.findViewById(R.id.editTextCP);
        editTextVille = (TextView) this.findViewById(R.id.editTextVille);
        editTextDateC = (TextView) this.findViewById(R.id.editTextDateC);
        editTextHeureS = (TextView) this.findViewById(R.id.editTextHeureS);
        editTextDateL = (EditText) this.findViewById(R.id.editTextDateL);
        editTextHeureL = (EditText) this.findViewById(R.id.editTextHeureL);
        editTextEtat = (EditText) this.findViewById(R.id.editTextEtat);

        editTextId.setText(c.getIdentifiant() + "");
        editTextNom.setText(c.getNom());
        editTextPrenom.setText(c.getPrenom());
        editTextTel.setText(c.getTelephone());
        editTextAdr.setText(c.getAdresse());
        editTextCP.setText(c.getCodePostal());
        editTextVille.setText(c.getVille());
        editTextDateC.setText(c.getDateCommande());
        editTextHeureS.setText(c.getHeureSouhaitLiv());
        if (c.getEtat() != 0) {
            editTextDateL.setText(c.getDateLivraison());
            editTextHeureL.setText(c.getHeureReelleLivraison());
            editTextEtat.setText(c.getEtat() + "");
        }

        annule = (Button) this.findViewById(R.id.btnAnnuler);
        annule.setOnClickListener(this);
        enreg = (Button) this.findViewById(R.id.btnEnregistrer);
        enreg.setOnClickListener(this);
        geoloc = (Button) this.findViewById(R.id.btnGeoloc);
        geoloc.setOnClickListener(this);

    }

    public void save(){
        dateL = editTextDateL.getText().toString();
        heureL = editTextHeureL.getText().toString();
        etat = Integer.parseInt(editTextEtat.getText().toString());
        c.setDateLivraison(dateL);
        c.setHeureReelleLivraison(heureL);
        c.setEtat(etat);
        client.updateClient(c);
        }

     public void finish(){
         intentAfficheListeClients = new Intent(this, AfficheListeClients.class);
         this.startActivity(intentAfficheListeClients);
     }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnEnregistrer:
                etat = Integer.parseInt(editTextEtat.getText().toString());
                if(etat < 1 || etat > 3){
                    Toast.makeText(this, "ERREUR : veuillez saisir un état valide !", Toast.LENGTH_LONG).show();
                }
                else{
                    this.save();
                    this.finish();
                }
                break;
            case R.id.btnAnnuler:
                this.finish();
                break;
            case R.id.btnGeoloc:
                intentGeolocalisation = new Intent(this, Geolocalisation.class);
                // id = this.getIntent().getExtras().getInt("id");
                intentGeolocalisation.putExtra("id", id);
                this.startActivity(intentGeolocalisation);
                break;
        }
    }
}