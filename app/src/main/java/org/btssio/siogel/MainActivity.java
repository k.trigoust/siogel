package org.btssio.siogel;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageButton imgIdentification, imgLivraison, imgSauvegarde, imgImport;
    Intent intentLivraison;
    // ClientDAO client = new ClientDAO(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgIdentification = (ImageButton) this.findViewById(R.id.imageIdentification);
        imgIdentification.setOnClickListener(this);
        imgLivraison = (ImageButton) this.findViewById(R.id.imageLivraison);
        imgLivraison.setOnClickListener(this);
        imgSauvegarde = (ImageButton) this.findViewById(R.id.imageSauvegarde);
        imgSauvegarde.setOnClickListener(this);
        imgImport = (ImageButton) this.findViewById(R.id.imageImport);
        imgImport.setOnClickListener(this);
        /*
        testBD();
        ArrayList<Client> lesClients = client.getClients();
        for(Client c : lesClients){
            Log.i("onCreate", "visu all client (nom + id) : " + c.getNom() + " - " + c.getIdentifiant());
        }
        */
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.imageIdentification:
                Toast.makeText(this, "En cours de développement", Toast.LENGTH_LONG).show();
                break;
            case R.id.imageLivraison:
                intentLivraison = new Intent(this, AfficheListeClients.class);
                this.startActivity(intentLivraison);
                break;
            case R.id.imageSauvegarde:
                Toast.makeText(this, "En cours de développement", Toast.LENGTH_LONG).show();
                break;
            case R.id.imageImport:
                Toast.makeText(this, "En cours de développement", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void testBD(){
        ClientDAO client = new ClientDAO(this);
        Client c1 = new Client();
        Client c2 = new Client(18, "nc4", "pc2", "adresse", "72000", "Le Mans", "0000000000", "01/02/2021", "10h00", "12h00", "10/02/2021", "signature", 3);
        Log.i("onCreate", "Client c1 : " + c1.toString());
        Log.i("onCreate", "Client c2 : " + c2.toString());
        client.addClient(c1);
        client.addClient(c2);
        Log.i("onCreate", "Client c1 visu by id : " + client.getClient(18));
        ArrayList<Client> lesClients = client.getClients();
        for(Client c : lesClients){
            Log.i("onCreate", "visu all client (nom + id) : " + c.getNom() + " - " + c.getIdentifiant());
        }
        c2.setHeureReelleLivraison("15h15");
        c2.setDateLivraison("12/02/2021");
        c2.setEtat(2);
        client.updateClient(c2);
        c2.setSignatureBase64("sign");
        client.updateSignClient(c2);
        Log.i("onCreate", "Client c1 update " + client.getClient(18));
    }
}