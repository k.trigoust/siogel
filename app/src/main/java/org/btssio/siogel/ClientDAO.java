package org.btssio.siogel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ClientDAO {

    private static String base = "BDClient";
    private static int version = 2;
    private BdSQLiteOpenHelper accesBD;

    public ClientDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);
    }

    public void addClient(Client unClient){
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        String req = "INSERT INTO client (nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv)"
                   + "VALUES('" + unClient.getNom() + "' , '" + unClient.getPrenom() + "' , '" + unClient.getAdresse() + "' , '" + unClient.getCodePostal() + "' , '" + unClient.getVille() + "' , '" + unClient.getTelephone() + "' , '" + unClient.getDateCommande() + "' , '" + unClient.getHeureSouhaitLiv() +"');";
        bd.execSQL(req);
        bd.close();
    }

    public Client getClient(int idClient){
        Client leClient = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("SELECT * FROM client WHERE identifiant ="+idClient+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leClient = new Client(idClient,curseur.getString(1), curseur.getString(2), curseur.getString(3), curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),curseur.getString(8), curseur.getString(9), curseur.getString(10),curseur.getString(11), curseur.getInt(12));
        }
        return leClient;
    }

    public ArrayList<Client> getClients(){
        Cursor curseur;
        String req = "SELECT * FROM client;";
        curseur = accesBD.getReadableDatabase().rawQuery(req, null);
        return cursorToClientArrayList(curseur);
    }

    private ArrayList<Client> cursorToClientArrayList(Cursor curseur){
        ArrayList<Client> listeClient = new ArrayList<Client>();
        int identifiant, etat;
        String nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv, heureReelleLivraison, dateLivraison, signatureBase64;

        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            identifiant = curseur.getInt(0);
            nom = curseur.getString(1);
            prenom = curseur.getString(2);
            adresse = curseur.getString(3);
            codePostal = curseur.getString(4);
            ville = curseur.getString(5);
            telephone = curseur.getString(6);
            dateCommande = curseur.getString(7);
            heureSouhaitLiv = curseur.getString(8);
            heureReelleLivraison = curseur.getString(9);
            dateLivraison = curseur.getString(10);
            signatureBase64 = curseur.getString(11);
            etat = curseur.getInt(12);
            listeClient.add(new Client(identifiant, nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv, heureReelleLivraison, dateLivraison, signatureBase64, etat));
            curseur.moveToNext();
        }

        return listeClient;
    }

    public boolean updateClient(Client c){
        SQLiteDatabase bd = accesBD.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("identifiant", c.getIdentifiant());
        value.put("heureReelleLivraison", c.getHeureReelleLivraison());
        value.put("dateLivraison", c.getDateLivraison());
        value.put("etat", c.getEtat());
        bd.update("client", value, "identifiant = ?", new String[] { String.valueOf(c.getIdentifiant())});
        bd.close();

        return true;
    }

    public boolean updateSignClient(Client c){
        SQLiteDatabase bd = accesBD.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("identifiant", c.getIdentifiant());
        value.put("signatureBase64", c.getSignatureBase64());
        bd.update("client", value, "identifiant = ?", new String[] { String.valueOf(c.getIdentifiant())});
        bd.close();

        return true;

    }

}
