package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AfficheListeClients extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    ClientDAO client;
    ArrayList<Client> listeClients;
    Intent intentModification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_liste_clients);
        listView = (ListView)findViewById(R.id.lvListe);
        client= new ClientDAO(this);
        listeClients = client.getClients();

        ClientAdapter clientAdapter = new ClientAdapter(this, listeClients);
        listView.setAdapter(clientAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Toast.makeText(getApplicationContext(),"Choix : "+listeClients.get(position).getIdentifiant(), Toast.LENGTH_LONG).show();
        intentModification = new Intent(this, ModificationClient.class);
        intentModification.putExtra("id",listeClients.get(position).getIdentifiant());
        this.startActivity(intentModification);
    }
}