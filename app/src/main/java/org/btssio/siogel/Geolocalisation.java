package org.btssio.siogel;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.location.Criteria.ACCURACY_FINE;
import static android.location.Criteria.POWER_HIGH;

public class Geolocalisation extends FragmentActivity implements OnMapReadyCallback {

    LocationManager locationManager;
    String fournisseur, adresse;
    LatLng positionClient, positionLivreur;
    Boolean geoLocLivreur, geoLocClient;
    LatLngBounds.Builder builder;
    GoogleMap mMap;
    int id;
    ClientDAO client;
    Client c = new Client();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocalisation);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        id = this.getIntent().getExtras().getInt("id");
        c = client.getClient(id);
        adresse = (c.getAdresse() + "," + c.getCodePostal() + " " + c.getVille() + " France");
        recupPositionLivreur();
        recupPositionClient();

        mapFragment.getMapAsync(this);
    }


    public void recupPositionLivreur(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteres = new Criteria();
        // Précision pour la localisation (Fine ou "grossière")
        criteres.setAccuracy(ACCURACY_FINE);
        // Conso d'énergie
        criteres.setCostAllowed(true);
        criteres.setPowerRequirement(POWER_HIGH);

        // Recherche du meilleur provider / fournisseur en fonction des critères, et récupération de la position :
        fournisseur = locationManager.getBestProvider(criteres, true);
        if(fournisseur == null || fournisseur.isEmpty()) { // si aucun fournisseur n'a été trouvé
            Toast.makeText(this, "Géolocalisation impossible", Toast.LENGTH_SHORT).show();
        }
        else {
        /* un fournisseur a été trouvé, il faut donc gérer la maj de la géolocalisation
        avec la méthode requestLocationUpdates qui prend en paramètres :
        - le fournisseur de position
        - la période entre 2 maj en millisecondes
        - la période entre 2 maj en mètres
        - l'écouteur qui sera lancé dès que le fournisseur sera activé
        */
            try {
                locationManager.requestLocationUpdates(fournisseur, 20000, 0, (LocationListener) this);
                // Dernière localisation connue
                Location location = locationManager.getLastKnownLocation(fournisseur);
                if(location != null){
                    // récup coordonnées
                    positionLivreur = new LatLng(location.getLatitude(), location.getLongitude());
                    geoLocLivreur = true;
                }
                else{
                    // Fausses coordonnées - par défaut
                    positionLivreur = new LatLng(47.958423, 0.166806);
                    geoLocLivreur = true;
                    Toast.makeText(this, "Erreur de localisation, coordonnées par défaut affichée",
                            Toast.LENGTH_LONG).show();
                }
            } catch (SecurityException e) {
                geoLocLivreur = false;
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }
    public void recupPositionClient(){
        Geocoder fwdGeocoder = new Geocoder(this, Locale.FRANCE);
        List<Address> locations = null;
        try{
            // dans la liste d'adresses à géocoder, il faut ajouter l'adresse du client composée précédemment
            locations = fwdGeocoder.getFromLocationName(this.adresse, 10);
        }
        catch (IOException e){
            Toast.makeText(this, "Erreur du géocodeur : " + e.getMessage(), Toast.LENGTH_LONG).show();
            geoLocClient = false;
        }
        if (locations.isEmpty() || locations == null){ // si l'adresse est inconnué (mauvaise adresse)
            Toast.makeText(this, "Adresse client introuvable", Toast.LENGTH_LONG).show();
            geoLocClient = false;
        }
        else {
            // adresse connue, on peut donc récupérer les Coordonnées de l'adresse client
            positionClient = new LatLng(locations.get(0).getLatitude(), locations.get(0).getLongitude());
            geoLocClient = true;
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(new MarkerOptions().
                position(positionClient).
                title("Client").
                snippet("Point de rendez-vous prochain client").
                icon(BitmapDescriptorFactory.fromResource(R.mipmap.grnpushpin)));
        builder.include(positionClient);

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(),
                                                            this.getResources().getDisplayMetrics().widthPixels,
                                                            this.getResources().getDisplayMetrics().heightPixels,
                                                            100));

       /*
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        */
    }
}