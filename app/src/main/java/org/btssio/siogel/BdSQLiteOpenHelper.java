package org.btssio.siogel;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {

    private String requete = "CREATE TABLE client ("
            + "identifiant INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "nom TEXT NOT NULL,"
            + "prenom TEXT NOT NULL,"
            + "adresse TEXT NOT NULL,"
            + "codePostal TEXT NOT NULL,"
            + "ville TEXT NOT NULL,"
            + "telephone TEXT NOT NULL,"
            + "dateCommande TEXT NOT NULL,"
            + "heureSouhaitLiv TEXT NOT NULL,"
            + "heureReelleLivraison TEXT,"
            + "dateLivraison TEXT,"
            + "signatureBase64 TEXT,"
            + "etat INTEGER);";

    public BdSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(requete);
        db.execSQL("INSERT INTO client (nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv) VALUES ('NOM1','Prenom1','Place du 8 Mai 1945','72000','Le Mans','0123456789','24/01/2021','10h00');");
        db.execSQL("INSERT INTO client (nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv) VALUES ('NOM2','Prenom2','8 Place George Washington','72100','Le Mans','0123456789','25/01/2021','12h00');");
        db.execSQL("INSERT INTO client (nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv) VALUES ('NOM3','Prenom3','2 Place Saint Michel','72000','Le Mans','0123456789','26/01/2021','13h00');");
        db.execSQL("INSERT INTO client (nom, prenom, adresse, codePostal, ville, telephone, dateCommande, heureSouhaitLiv) VALUES ('NOM4','Prenom4','Place De La Mairie','72560','Changé','0123456789','27/01/2021','14h00');");
        ContentValues value = new ContentValues();
        value.put("nom", "MALRAUX");
        value.put("prenom","André");
        value.put("adresse","3 Rue de Beau Soleil");
        value.put("codePostal","72700");
        value.put("ville","Allonnes");
        value.put("telephone","0123456789");
        value.put("dateCommande","28/01/2021");
        value.put("heureSouhaitLiv","15h00");
        db.insert("client", null, value);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS client");
        onCreate(db);
    }
}
